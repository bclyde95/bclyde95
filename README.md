# 👋 Hi, I’m @bclyde95
- 👀 I’m interested in IOT, Linux, hardware design, automation, and web dev.
- 🌱 I’m constantly learning and honing skills adjacent to my current ones. However, in the future I'd like to deep dive into AI and Machine Learning.
- 📫 How to reach me: email me at bclyde9514@gmail.com.

<!---
bclyde95/bclyde95 is a ✨ special ✨ repository because its `README.md` (this file) appears on your GitHub profile.
You can click the Preview link to take a look at your changes.
--->
